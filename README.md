This project is designed to handle requests from DevOps External [feedback form](https://devops.pan-net.cloud/feedback/]).  
Service Desk feature is enabled for this project.  
Issues will be automatically generated from incoming emails received to the email address, provided by Gitlab.com (Find [here](https://gitlab.com/pan-net1/devops-team-service-desk/-/issues/service_desk))


There are two types of built-in automatic responses provided to the customer.
Email body templates are stored in `.gitlab/service_desk_templates/` directory of this repository.

* `thank_you.md` - the email sent to a user after they submit a new issue.
* `new_note.md` - the email sent to a user when the issue they submitted has a new comment.

The following placeholders are applicable in templates:  
`%{ISSUE_ID}`  
`%{ISSUE_PATH}`  
`%{NOTE_TEXT}`  



More information in Gitlab Service Desk [official documentation](https://docs.gitlab.com/ee/user/project/service_desk.html) 